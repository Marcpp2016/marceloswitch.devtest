﻿using NUnit.Framework;

/*
-------------------
    Marcelo Borello
    marartb1@gmail.com
    Test for Switch
    09/05/19
-------------------

*/


namespace SharpBank.Test
{
    [TestFixture]
    public class TransactionTest
    {
        [Test]
        public void Transaction()
        {
            Transaction t = new Transaction(50);

            Assert.AreEqual(t.amount, 50);
            Assert.AreEqual(t.GetTransactionDate(), DateProvider.GetInstance().Now());

        


        }
    }
}
