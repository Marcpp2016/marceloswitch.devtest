﻿

using NUnit.Framework;

/*

-------------------
    Marcelo Borello
    marartb1@gmail.com
    Test for Switch
    09/05/19
-------------------

*/
namespace SharpBank.Test
{

    [TestFixture]
    class AccountTest
    {
        [Test]
        public void TestInterestEarned()
        {
            Account ac = new Account(Account.SAVINGS);
            ac.Deposit(3000.0);
            Assert.AreEqual(5.0, ac.InterestEarned());
            

        }

        [Test]
        public void TestSumTransactions()
        {
            Account ac = new Account(Account.SAVINGS);
            ac.Deposit(5000.0);
            Assert.AreEqual(5000.0, ac.SumTransactions());
        }



        [Test]
        public void TestDeposit()
        {
            Account ac = new Account(Account.SAVINGS);
            ac.Deposit(5000.0);
            Assert.AreEqual(5000.0, ac.Amount);
        }


        [Test]
        public void TestWithdraw()
        {
            Account ac = new Account(Account.SAVINGS);
            ac.Deposit(5000.0);
            ac.Withdraw(4000.0);
            Assert.AreEqual(1000.0, ac.Amount);

        }
        

    }
}
