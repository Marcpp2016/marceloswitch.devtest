﻿using NUnit.Framework;

/*
-------------------
    Marcelo Borello
    marartb1@gmail.com
    Test for Switch
    09/05/19
-------------------

*/


namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {



        [Test]
        public void TestOneAccount()
        {
            Customer Ramon = new Customer("Ramon").OpenAccount(new Account(Account.SAVINGS));
            Assert.AreEqual(1, Ramon.GetNumberOfAccounts());
        }
        
        [Test]
        public void TestTwoAccount()
        {
            Customer Ramon = new Customer("Ramon").OpenAccount(new Account(Account.SAVINGS));
            Ramon.OpenAccount(new Account(Account.CHECKING));
            Assert.AreEqual(2, Ramon.GetNumberOfAccounts());
        }

        [Test]
        public void TestThreeAcounts()
        {
            Customer Alberto = new Customer("Alberto").OpenAccount(new Account(Account.SAVINGS));
            Alberto.OpenAccount(new Account(Account.CHECKING));
            Alberto.OpenAccount(new Account(Account.MAXI_SAVINGS));
            Assert.AreEqual(3, Alberto.GetNumberOfAccounts());
        }
        [Test]
        public void TesTodollar()
        {
            Customer Pedro = new Customer("Pedro");
            Assert.AreEqual("$200.00", Pedro.ToDollars(200.00));
        }
        [Test]
        public void TestTBetweenAccounts()
        {
            Customer Juan = new Customer("Juan");
            Assert.AreEqual(true, Juan.TBetweenAccounts(new Account(Account.CHECKING), new Account(Account.CHECKING), 2000.0));
        }

        [Test]
        public void TestTGetStatmentAmount()
        {
            //String statment
            string statmentGive = "Statement for PedroChecking Account  deposit $0.00\n  deposit $100.00\nTotal $100.00Savings Account  deposit $0.00\n  deposit $4,000.00\n  withdrawal $200.00\nTotal $3,800.00 Total In All Accounts $3,900.00";

            //Create Accounts 
            Account checking = new Account(Account.CHECKING);
            Account savings = new Account(Account.SAVINGS);
            //Open two Acoounts and pass them by parameters de Accounts
            Customer Pedro = new Customer("Pedro").OpenAccount(checking).OpenAccount(savings);

            //Checking deposit
            checking.Deposit(100.0);
            //Savings deposit
            savings.Deposit(4000.0);
            //Withdraw from Account
            savings.Withdraw(200.0);

            //Run Assert 
            Assert.AreEqual(statmentGive, Pedro.GetStatement());
            
        }

        /*
    

        
        */

    }
}
