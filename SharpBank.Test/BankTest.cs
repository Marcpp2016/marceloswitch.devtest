﻿using NUnit.Framework;

/*
-------------------
    Marcelo Borello
    marartb1@gmail.com
    Test for Switch
    09/05/19
-------------------

*/

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
         private static readonly double DOUBLE_DELTA = 1e-15;

        [Test]
        public void CustomerSummary()
        {
            Bank bank = new Bank();
            Customer Robert = new Customer("Robert");
            Robert.OpenAccount(new Account(Account.CHECKING));
            bank.AddCustomer(Robert);
            Assert.AreEqual("Customer Summary Robert=> 1 account ", bank.CustomerSummary());
        }

      

        [Test]
        public void SavingsAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(Account.SAVINGS);
            bank.AddCustomer(new Customer("Carlos").OpenAccount(checkingAccount));
            checkingAccount.Deposit(1500.0);
            Assert.AreEqual(2.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }
        [Test]
        public void MaxiSavingsAccountWithdrawal()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(Account.MAXI_SAVINGS);
            bank.AddCustomer(new Customer("Jhon").OpenAccount(checkingAccount));
            checkingAccount.Deposit(41.0);
            checkingAccount.Withdraw(1.0);
            Assert.AreEqual(2.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
         }
        [Test]
        public void CheckingAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(Account.CHECKING);
            Customer Juan = new Customer("Juan").OpenAccount(checkingAccount);
            bank.AddCustomer(Juan);
            checkingAccount.Deposit(150000);
            Assert.AreEqual(150.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }
    }
}
