﻿using System;
using System.Collections.Generic;

/*
-------------------
    Marcelo Borello
    marartb1@gmail.com
    Test for Switch
    09/05/19
-------------------
---------------------------
         Bank
---------------------------
*/



namespace SharpBank
{
    public class Bank
    {

        //List Customers
        private List<Customer> customers;

        //Constructor Bank
        public Bank()
        {
            customers = new List<Customer>();
        }
        //Add Customer
        public void AddCustomer(Customer customer)
        {
            customers.Add(customer);
            
            if (customer == null)
            {
                throw new Exception("not customer");
            }
        }
        //Sumary Customer
        public String CustomerSummary()
        {
            String summary = "Customer Summary";

            foreach (Customer c in customers)
            {
               
                summary +=" "+ c.GetName()+"=> "+Format(c.GetNumberOfAccounts(), "account")+" " ;
            }
                return summary;
        }

        private String Format(int number, String word)
        {
            return number + " " + (number == 1 ? word : word + "s");
        }

        //Total Interest Paid
        public double TotalInterestPaid()
        {
            double total = 0;

            foreach (Customer c in customers)
                total += c.TotalInterestEarned();
            return total;
        }

        //Get First Customer
        public String GetFirstCustomer()
        {
            try
            {
                customers = null;
                return customers[0].GetName();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return "Error";
            }
        }
    }
}
