﻿using System;


/*
-----------------------------------------------
     Data provider Class
     Patron singleton 
  
------------------------------------------------
*/

namespace SharpBank
{
    public class DateProvider  
    {
        //Instance this clase
        private static DateProvider instance = null;
        private   DateTime timeNow;
        
        //this bool giveus instance bool 
        private static  bool miInstance=false;
      
        //Encapsulate Constructor only Access from this class
       private DateProvider(){ }


        //Getter of this Class instance
        public static DateProvider GetInstance()
        {
            if (instance == null)
            {

                instance = new DateProvider();
                miInstance = true;
            }
            else {
                miInstance = false;
            }
            return instance;
        }
        
        //Date  Actual Time
        public  DateTime Now()
        {
            // Chanche time bycost error in seconds test
            return timeNow;
        }

        
        public static bool GetmiIntanceBool()
        {
            
            return miInstance;
        }
    }
}
