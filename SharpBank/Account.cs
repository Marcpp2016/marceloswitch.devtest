﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
-------------------
    Marcelo Borello
    marartb1@gmail.com
    Test for Switch
    09/05/19
-------------------
-------------------------
    Account 
-----------------------
*/



namespace SharpBank
{
    public class Account
    {
        //Cons
        public const int CHECKING = 0;
        public const int SAVINGS = 1;
        public const int MAXI_SAVINGS = 2;
        //Amount
        private double amount;


        private readonly int accountType;
      
        //List  Transaction
        public List<Transaction> transactions;
        


        // Constructor
        public Account(int accountType)
        {
            this.accountType = accountType;
            this.transactions = new List<Transaction>();
            this.transactions.Add(new Transaction(amount));
            this.amount += amount;
           

        }

        //Deposit
        public void Deposit(double amount)
        {
            Transaction tr = new Transaction(amount);
            if (amount <= 0)
            {
                throw new Exception("this amount  isnt  greater than zero");
            }
            else
            {
                transactions.Add(tr);
                this.amount += amount;
            }
        }

        //Withdraw
        public void Withdraw(double amount)
        {
            Transaction tr = new Transaction(-amount);
            
                if (amount <= 0)
            {
                throw new Exception("amount its no  greater than zero");
            }
            else if (amount >= this.Amount)
            {
                throw new Exception("you dont have money  in your account");
            }
            else

            {

                transactions.Add(tr);
                this.amount += -amount;
            }

        }

        //InterestEarned
        public double InterestEarned()
        {
            double amount = SumTransactions();

            switch (accountType)
            {
                case SAVINGS:

                    if (amount <= 1000)
                    {
                        return amount * 0.001;
                    }
                    else
                    {
                        return 1 + (amount - 1000) * 0.002;
                    }
                case MAXI_SAVINGS:
                    if (CheckIfWithdrawalExist(10))
                    {
                        return amount * 0.001;
                    }
                    else
                    {
                        return amount * 0.05;
                    }
                default:
                    return amount * 0.001;
            }
        }

        //SumTransactions
        public double SumTransactions()
        {
            double amount = 0;

            foreach (Transaction t in transactions)
            {
                amount += t.amount;
            }
            return amount;
        }
        public bool CheckIfWithdrawalExist(int days)
        {
            while (days >= 0)
            {
                foreach (Transaction t in transactions)
                {
                    if (t.GetTransactionDate() == (DateTime.Today.AddDays(-days).Date))

                        if (t.amount < 0)
                            return true;
                }
                days--;
            }
            return false;
        }

        //Get Account Type
        public int GetAccountType()
        {
            return accountType;
        }

        //Property Amount
        public double Amount
        {
            get
            {
                return amount;
            }
            set
            {
                amount = value;
            }

        }




    }
}
