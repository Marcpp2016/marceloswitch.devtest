﻿using System;

/*
-------------------
    Marcelo Borello
    marartb1@gmail.com
    Test for Switch
    09/05/19
-------------------
-----------------------------
Transaction Class
----------------------------
*/

namespace SharpBank
{
    public class Transaction
    {
        //Amount
        public  readonly double amount;
      
        //Time Date
        private DateTime transactionDate;
      
        //Transaction
        public Transaction(double amount)
        {
            this.amount = amount;
            this.transactionDate = DateProvider.GetInstance().Now();
        }

        //Get Transaction Date
        public DateTime GetTransactionDate()
        {
            return transactionDate;
        }

       
    }
}
