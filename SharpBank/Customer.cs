﻿using System;
using System.Collections.Generic;

/*
-------------------
    Marcelo Borello
    marartb1@gmail.com
    Test for Switch
    09/05/19
-------------------
Customer class
--------------------
*/
namespace SharpBank
{

    public class Customer
    {
        //Name Customer
        private String name;
        
        //List Accounts
        private List<Account> accounts;

        //Constructor
        public Customer(String name)
        {
            this.name = name;
            accounts = new List<Account>();
        }

        //Get Name
        public String GetName()
        {
            return name;
        }

        //Open Customer
        public Customer OpenAccount(Account account)
        {
            accounts.Add(account);

            if (accounts==null)
            {
                throw new ArgumentException(" Customer not have Account open");

            }
            return this;
        }

        //Get Number of Accounts
        public int GetNumberOfAccounts()
        {
            return accounts.Count;
        }
        
        //Trasfer Between Accounts
        public bool TBetweenAccounts(Account account1, Account account2, double amount)
        {
            bool tranBtween=false;

            account1.Amount = amount;
            account2.Amount = amount;
            
            if (account1.Amount >= 0)
            {
                
                account1.Withdraw(100);
                account2.Deposit(200);
                tranBtween = true;
            }
            else {
                tranBtween = false;
                throw new Exception("account_1 dont have money ");
            }
            return tranBtween;
        }

        //Total Interest Earned
        public double TotalInterestEarned()
        {
            double total = 0;
            foreach (Account a in accounts)
                total += a.InterestEarned();
            return total;
        }

        /*******************************
         * This method gets a statement
         *********************************/
        public String GetStatement()
        {

            String statement = null; 
           
            statement = "Statement for "+name ;

            double total = 0.0;

            foreach (Account a in accounts)
            {
                statement +=StatementForAccount(a) ;
                total += a.SumTransactions();
            }

            statement +=" Total In All Accounts" +" "+ToDollars(total);
            return statement;
        }

        //Statement For Account
        private String StatementForAccount(Account a)
        {
            String s = "";

            //Translate to pretty account type
            switch (a.GetAccountType())
            {
                case  Account.CHECKING:
                    s += "Checking Account";
                    break;
                case Account.SAVINGS:
                    s += "Savings Account";
                    break;
                case Account.MAXI_SAVINGS:
                    s += "Maxi Savings Account";
                    break;
            }

            //Now Total up all the Transactions
            double total = 0.0;

            foreach (Transaction t in a.transactions)
            {
                s += "  " + (t.amount < 0 ? "withdrawal" : "deposit") + " " + ToDollars(t.amount) + "\n";
                total += t.amount;
            }
            s += "Total " + ToDollars(total);
            return s;
        }

        //To Dollars Money
      public String ToDollars(double d)
        {
            return String.Format("${0:N2}",Math.Abs(d));
        }
    }
}
